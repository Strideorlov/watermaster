#include <ESP8266WiFi.h>
#include <Wire.h>
#include <PubSubClient.h>


#define wifi_ssid "bagarstugan1337"
#define wifi_password "Tracktor01"

#define mqtt_server "192.168.2.212"
#define mqtt_user "water"
#define mqtt_password "water12"

#define humidity_topic "Pachira aquatica/humidity"



WiFiClient espClient;
PubSubClient client(espClient);


// setting you should lift out to a web servic later

int pumpTime = 10; // pump timein seconds
int moistSettings = 10; // how low moister lvl before waterin is initiated in %
int TimeMoistLvl = 60; //how long time it should be dry before watering is init in minutes

// HW setting this needs to be changed if rewiring is done
int analogPin = A0;
int moist = 0;
int motorPin = D1;

  
void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
  Serial.println();
pinMode(motorPin, OUTPUT);


  setup_wifi();
  client.setServer(mqtt_server, 1883);
   client.setCallback(callback);
   

}


void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      Serial.println("connected");
      client.subscribe("moistSettings");
      client.subscribe("pumpTime");
      client.subscribe("TimeMoistLvl");
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {

  if (!client.connected()) {
  reconnect();
  }
  client.loop();
  
int i =0;
int mosit = moistRead();
Serial.println();
Serial.print("moistSettings % ");
Serial.println(moistSettings);




while (mosit <= moistSettings){
  
  Serial.print("itration: ");
  Serial.println(i);
  
   if (i >= TimeMoistLvl) {
    motor(pumpTime);
    Serial.println("inside motor");
   }
   i++;
   delay(60000);
   mosit = moistRead();
 }  
 
  float temp = mosit;
  Serial.print("New humidity:");
  Serial.println(String(temp).c_str());
  client.publish(humidity_topic, String(temp).c_str(), true);
  

 
delay(10000);
}

int moistRead (){
int value = analogRead(analogPin);
int val = map(value, 300, 630 , 100 ,0);
 return val;
}

void motor(int y) {
int  x = 0;
  while (x < y){
    digitalWrite(motorPin, HIGH);
    x = x+1 ;
    delay(1000);
    Serial.print(x);
  } 
  digitalWrite(motorPin, LOW); 
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String topicStr(topic);
  if (topicStr == "moistSettings"){
    moistSettings = atoi ((char*)payload);
    Serial.print(moistSettings);
  }
  if (topicStr == "pumpTime"){
    pumpTime = atoi ((char*)payload);
    Serial.print(pumpTime);
  }
  if(topicStr == "TimeMoistLvl"){
    TimeMoistLvl = atoi ((char*)payload);
    Serial.print(TimeMoistLvl);   
  }
  }
